## Resumen

(Da el resumen del issue)

## Pasos para reproducirlo

(Indica los pasos para reproducir el bug)

## ¿Cuál es el comportamiento actual?

(Indica el comportamiento actual)

## ¿Cuál es el comportamiento esperado?

(Indica el comportamiento esperado)
