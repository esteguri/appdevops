/*global test, expect, React*/

import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders AppDevOps link", () => {
  render(<App />);
  const linkElement = screen.getByText(/AppDevOps/i);
  expect(linkElement).toBeInTheDocument();
});
