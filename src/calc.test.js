/* global describe, test, expect */

import { Calc } from "./calc";

describe("Test to calc", () => {
  test("should sum 2 values", () => {
    expect(Calc.sum(1, 2)).toBe(3);
  });
});
